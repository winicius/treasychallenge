# FROM maven:3.3-jdk-8 as builder
# WORKDIR /usr/src/tree
# # VOLUME m2_data:/root/.m2/

# # Copy pom and install dependencies
# COPY pom.xml /usr/src/tree
# RUN mvn verify --fail-never

# # Copy rest of the project
# COPY . /usr/src/tree
# RUN mvn clean install -f /usr/src/tree && mkdir /usr/src/wars/
# RUN find /usr/src/tree/ -iname '*.war' -exec cp {} /usr/src/wars/ \;
# RUN ls /usr/src/wars/

# runtime layer
FROM tomcat:9-jre8-alpine
RUN rm -rf /usr/local/tomcat/webapps/ROOT
COPY ./target/tree.war /usr/local/tomcat/webapps/ROOT.war
# COPY --from=builder /usr/src/wars/*.war /usr/local/tomcat/webapps
RUN ls /usr/local/tomcat/webapps/

