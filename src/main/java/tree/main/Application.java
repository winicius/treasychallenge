package tree.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
@EntityScan(basePackages = { "tree.model" })
@EnableJpaRepositories(basePackages = { "tree.repository" })
@ComponentScan(basePackages = { "tree.controller" })
public class Application extends SpringBootServletInitializer {

	@GetMapping(value="/hello")
	public String getMethodName() {
		return "Hello world";
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}

