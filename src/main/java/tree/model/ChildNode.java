package tree.model;

public class ChildNode {

    public Long id;

    public String code;

    public String description;

    public String detail;

    public Long parentId;

    public boolean hasChildren;

    private ChildNode() {
    }

    public ChildNode(long id, String code, String description, Long parentId, String detail, boolean hasChildren) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.parentId = parentId;
        this.detail = detail;
        this.hasChildren = hasChildren;
    }

    public long getId() {
        return this.id;
    }
}
