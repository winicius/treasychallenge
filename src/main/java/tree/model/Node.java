package tree.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class Node {

    @Id
    @GeneratedValue
    private Long id;

    public String code;

    public String description;


    public String detail;

    public Long parentId;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "parentId")
    public List<Node> children;


    private Node() {
    }

    public Node(String code, String description, Long parentId, String detail) {
        this.code = code;
        this.description = description;
        this.parentId = parentId;
        this.detail = detail;
    }

    public Long getId() {
        return this.id;
    }
}
