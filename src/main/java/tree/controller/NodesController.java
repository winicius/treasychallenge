package tree.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tree.exceptions.NodeNotFoundException;
import tree.model.ChildNode;
import tree.model.Node;
import tree.repository.NodeRepository;

@RestController
public class NodesController {

    private NodeRepository repository;

    public NodesController(NodeRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/node")
    public Iterable<Node> getAll() {
        return this.repository.findAll();
    }

    @GetMapping("/node/{id}")
    public List<ChildNode> get(@PathVariable("id") Long id) {

        if (id == null) {
            throw new NullPointerException("null parameter");
        }

        Optional<Node> value = this.repository.findById(id);

        if (value.isPresent()) {
            List<ChildNode> children = value.get().children.stream().map(x -> {
                boolean hasChildren = !x.children.isEmpty();
                long objId = x.getId();
                ChildNode cNode = new ChildNode(objId, x.code, x.description, x.parentId, x.detail, hasChildren);
                return cNode;
            }).collect(Collectors.toList());

            return children;
        } else {
            throw new NodeNotFoundException();
        }
    }

    @PostMapping("/node")
    public long add(@RequestBody Node node) {
        Node res = repository.save(node);
        return res.getId();
    }

    @PutMapping("/node")
    public long Update(@RequestBody Node nodeReq) {

        if (nodeReq == null) {
            throw new NullPointerException("null body content");
        }

        Optional<Node> node = this.repository.findById(nodeReq.getId());

        if (node.isPresent()) {

            Node value = node.get();
            value.code = nodeReq.code;
            value.description = nodeReq.description;
            value.detail = nodeReq.detail;
            value.parentId = nodeReq.parentId;
            this.repository.save(value);
            return value.getId();
        } else {
            throw new NodeNotFoundException();
        }
    }

}
